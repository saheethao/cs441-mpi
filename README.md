# MPI vs Threads vs Serial (Only one will survive)
## Date:
12/17/2018

## Description:
These are <i>Hello World!</i> programs in their respective usages.

## How to build the software
### MPI
```
mpicc <file name> -o <executable file name>
```
### Thread
```
gcc <file name> -o <executable file name> -lpthread
```
### Serial
```
gcc <file name> -o <executable file name>
```
## How to use the software
### MPI
```
module load mpi/openmpi-x86_64

mpiexec -np <number or processes> ./<executable file name>
```
### Thread
```
./<executable file name>
```
### Serial
```
./<executable file name>
```

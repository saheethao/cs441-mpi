#include <stdio.h>
int main(int argc, char ** argv) {
  int num = 5;
  int i;
  for (i = 0; i <= num; i += 1) {
    if (i % 2 == 0) {
      printf("Hello World! I'm iteration %d. I am even.\n", i, num);
    } else {
      printf("Hello World! I'm iteration %d. I am odd.\n" , i, num);
    }
  }
  return 0;
}

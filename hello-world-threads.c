#include <stdio.h>
#include <pthread.h>

void *helloWorld(void *threadid) {
  int tid = (intptr_t) threadid;
  if (tid % 2 == 0) {
    printf("Hello World! I'm thread %d. I am even.\n", tid);
  } else {
    printf("Hello World! I'm thread %d. I am odd.\n" , tid);
  }
  pthread_exit(NULL);
}

int main(int argc, char * argv[]) {
  int num = 5;
  pthread_t threads[num];
  int rc, i;
  for (i = 0; i < num; i += 1) {
    rc = pthread_create(&threads[i], NULL, helloWorld, (void *)(intptr_t)i);
  }
  return 0;
}

/*
 * Date last modified: 10/30/2018
 * Author: Sahee Thao
 * CS 441/541: Hello world
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>
#include "semaphore_support.h"

/*****************************
 * Defines
 *****************************/
 #define MAX_VAL 10;

/*****************************
 * Structures
 *****************************/


/*****************************
 * Global Variables
 *****************************/
int sleep_time; /* How long main sleeps */
int num_producers; /* Number of producers */
int num_consumers; /* Number of consumers */
int bufsize; /* Size of the buffer */

int *buffer; /* Array of items that producers and consumers */

int is_done; /* Specifically is done sleeping */

int pro_count; /* Count of how many productions have been made */
int con_count; /* Count of how many consumptions have been taken */

semaphore_t pro_mutex; /* Mutex for accessing pro_count */
semaphore_t con_mutex; /* Mutex for accessing con_count */
semaphore_t print_mutex; /* Mutex for print_buffer() */
semaphore_t produced_space; /* Number of spaces with items in the buffer */
semaphore_t empty_space; /* Number of spaces with no items in the buffer */

/*****************************
 * Function Declarations
 *****************************/

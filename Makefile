#
# Samantha Foley
# CS 441/541: Project 4
#
#####################################################################
#
# Type "make" to compile your code
#
# Type "make clean" to remove the executable (and any object files)
#
#####################################################################

CC=mpicc
#CFLAGS=-Wall -g -O0 -Ilib
#LDFLAGS=-pthread

PROGS=hello-world
#
# List all of the binary programs you want to build here
# Separate each program with a single space
#
all: $(PROGS)

#
# Bounded Buffer
#
hello-world: hello-world.c
	$(CC) hello-world.c -o hello-world 

#
# Cleanup the files that we have created
#
clean:
	$(RM) $(PROGS) *.o
	$(RM) -rf *.dSYM
	cd lib && make clean

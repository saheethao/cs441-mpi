import mpi.*;

public class HelloWorld {
  public static void main(String args[]) {
    try {
      MPI.Init(args);
      int myId = MPI.COMM_WORLD.Rank();
      int size = MPI.COMM_WORLD.Size();
      if (myId % 2 == 0) {
        System.out.println("Hello world! I'm process " + myId + ". I am even.");
      } else {
        System.out.println("Hello world! I'm process " + myId + ". I am odd." );
      }
    } catch (Exception e) {
      e.printstacktrace()
    }
  }
}
